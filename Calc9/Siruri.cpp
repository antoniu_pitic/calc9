#include "Siruri.h"

void Pasareste(char S[100], char P[300])
{
	int j = 0;

	for (int i = 0; S[i]; i++)
	{
		P[j++] = S[i];
		if (S[i]=='a' || S[i]=='e' || S[i]=='i' || 
			S[i]=='o' || S[i]=='u') {
			P[j++] = 'p';
			P[j++] = S[i];
		}
	}
	P[j] = 0;
}

void Depasareste(char P[300], char D[100])
{
	int j = 0;

	for (int i = 0; P[i]; i++)
	{
		D[j++] = P[i];
		if (P[i] == 'a' || P[i] == 'e' || P[i] == 'i' ||
			P[i] == 'o' || P[i] == 'u') {
			i += 2;
		}
	}
	D[j] = 0;
}
